import by.epam.training.beans.Animal;
import by.epam.training.beans.EyeColorEnum;
import by.epam.training.beans.AnimalTypeEnum;
import by.epam.training.beans.Person;
import by.epam.training.interfaces.ActionInterface;
import by.epam.training.utilites.Equaler;
import by.epam.training.utilites.ClassBuilder;

import java.lang.reflect.InvocationTargetException;

public class Main {
    public static void main(String[] args) throws NoSuchFieldException,
            InvocationTargetException, NoSuchMethodException,
            IllegalAccessException, InstantiationException, ClassNotFoundException {

        ActionInterface person = (ActionInterface) ClassBuilder.getInstance(Person.class);
        StringBuffer string = (StringBuffer) ClassBuilder.getInstance(StringBuffer.class);
        string.append("Appended");
        person.voice();
        System.out.println(string + " not annot");

        Animal cat = new Animal(AnimalTypeEnum.CAT, EyeColorEnum.YELLOW, 3);
        Animal dog = new Animal(AnimalTypeEnum.DOG, EyeColorEnum.BLUE, 6);
        Animal secondCat = new Animal(AnimalTypeEnum.CAT, EyeColorEnum.YELLOW, 3);
        System.out.println("\n Animals are the same? " + Equaler.equalObjects(cat, dog));
        System.out.println("Animals are the same?  " + Equaler.equalObjects(cat, secondCat));

    }
}
