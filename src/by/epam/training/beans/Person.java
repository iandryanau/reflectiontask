package by.epam.training.beans;

import by.epam.training.annotations.Proxy;
import by.epam.training.interfaces.ActionInterface;
import by.epam.training.utilites.InvokeLogger;

@Proxy(invocationHandler = InvokeLogger.class)
public class Person implements ActionInterface {

    @Override
    public void voice() {
        System.out.println("Voicing");
    }
}
