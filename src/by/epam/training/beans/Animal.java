package by.epam.training.beans;


import by.epam.training.annotations.Equal;

public class Animal {

    private AnimalTypeEnum type;

    @Equal(compareType = Equal.CompareType.VALUE)
    private EyeColorEnum color;

    @Equal(compareType = Equal.CompareType.REF)
    private Integer age;


    public Animal(AnimalTypeEnum type, EyeColorEnum color, int age) {
        this.type = type;
        this.color = color;
        this.age = age;
    }

}
