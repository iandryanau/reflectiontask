package by.epam.training.beans;

public enum EyeColorEnum {
    GREEN, BLUE, YELLOW, GRAY
}
