package by.epam.training.utilites;


import by.epam.training.annotations.Equal;

import java.lang.reflect.Field;

public class Equaler {
    public static boolean equalObjects(Object obj1, Object obj2) throws IllegalAccessException {
        if (obj1 == obj2) {
            return true;
        }
        if (!obj1.getClass().equals(obj2.getClass())) {
            return false;
        }
        Field[] fields = obj1.getClass().getDeclaredFields();
        boolean result = false;
        for (Field field : fields) {
            if (field.isAnnotationPresent(by.epam.training.annotations.Equal.class)) {
                field.setAccessible(true);
                by.epam.training.annotations.Equal.CompareType compareType = field.getAnnotation(by.epam.training.annotations.Equal.class).compareType();
                switch (compareType) {
                    case REF:
                        result = field.get(obj1) == field.get(obj2);
                        break;
                    case VALUE:
                        result = field.get(obj1).equals(field.get(obj2));
                        break;
                }
                field.setAccessible(false);
                if (!result) {
                    return result;
                }
            }
        }
        return result;
    }
}