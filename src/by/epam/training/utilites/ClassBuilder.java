package by.epam.training.utilites;


import by.epam.training.annotations.Proxy;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;

public class ClassBuilder {
    public static Object getInstance(Class<?> clas) throws ClassNotFoundException,
            IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
        Object result = clas.newInstance();
        if (clas.isAnnotationPresent(Proxy.class)) {
            Class<?>[] interfaces = clas.getInterfaces();
            Proxy proxyAnnotation = clas.getAnnotation(Proxy.class);
            Class<?> classHandler = proxyAnnotation.invocationHandler();
            Constructor constructor = classHandler.getConstructor(Object.class);
            InvocationHandler handler = (InvocationHandler) constructor.newInstance(result);
            result = java.lang.reflect.Proxy.newProxyInstance(clas.getClassLoader(), interfaces, handler);
        }
        return result;
    }
}