package by.epam.training.utilites;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class InvokeLogger implements InvocationHandler {

    private Object obj;

    public InvokeLogger(Object obj) {
        this.obj = obj;
    }


    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println(method.getName() + " invoked");
        return method.invoke(obj, args);
    }
}
